Welcome to the project page for Alex Ruthmann's artistic research residency at IRCAM - Spring 2020.

Progress from local design workshops and software prototypes will be shared in this space. If you are looking for web music software from the NYU Music Experience Design Lab (MusEDLab), it can be found at the following links:
* https://apps.musedlab.org/aqwertyon/theory/
* https://apps.musedlab.org/groovepizza/
* https://apps.musedlab.org/variation-playground/

Other links to MusEDLab materials can be found at https://musedlab.org/.

